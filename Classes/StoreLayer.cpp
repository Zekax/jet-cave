#include "StoreLayer.h"
#include "ColorPickerLayer.h"
#include "ShapesPickerLayer.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

#if   (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "iOSHelper.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AdmobHelper.h"
#endif


USING_NS_CC;


Store::Store()
{
    LayerColor::initWithColor(Color4B(225, 225, 225, 2));
    
}


Store::~Store()
{
    
}

bool Store::init(int myScore, MainMenu *root)
{
    parent = root;
    
    insertObjects();
    
    return true;
}

void Store::insertObjects(){
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    UserDefault *def = UserDefault::getInstance();
    
    int R = def->getIntegerForKey("RED", 57);
    int G = def->getIntegerForKey("GREEN", 150);
    int B = def->getIntegerForKey("BLUE", 219);
    
    Color3B color = Color3B(R,G,B);
    
    auto label = Label::createWithTTF("STORE", "Aller_Bd.ttf", 32);
    
    label->setPosition(Vec2(origin.x + visibleSize.width / 2,origin.y + visibleSize.height - label->getContentSize().height));
    label->setTextColor(Color4B(color));
    this->addChild(label, 1);
        
    auto buttonLayout = ui::Layout::create();
    buttonLayout->setSize(visibleSize / 2);
    
    Button::Button("NO ADS", color, buttonLayout, Size(origin.x +visibleSize.width / 2 -100, origin.y +visibleSize.height / 2+70), 1, CC_CALLBACK_2(Store::GoToNoAds, this), NORMAL, "Aller_Bd.ttf", 32, 200);
    Button::Button("RESTORE\n      IAP", color, buttonLayout, Size(origin.x +visibleSize.width / 2 -100, origin.y +visibleSize.height / 2 - 60), 1, CC_CALLBACK_2(Store::GoToRestoreIap, this), NORMAL, "Aller_Bd.ttf", 22, 200);
    Button::Button("COLOR", color, buttonLayout, Size(origin.x +visibleSize.width / 2 +100, origin.y +visibleSize.height / 2 - 60), 1, CC_CALLBACK_2(Store::GoToColor, this), NORMAL, "Aller_Bd.ttf", 32, 200);
    Button::Button("SHAPES", color, buttonLayout, Size(origin.x +visibleSize.width / 2 +100, origin.y +visibleSize.height / 2 +70), 1, CC_CALLBACK_2(Store::GoToShapes, this), NORMAL, "Aller_Bd.ttf", 32, 200);
    Button::Button("", color, buttonLayout, Size(origin.x +visibleSize.width / 2 -225, origin.y +visibleSize.height / 2 + 70), -0.2, CC_CALLBACK_2(Store::ReturnMain, this), ARROW, "Aller_Bd.ttf", 32, 200);
    
    this->addChild(buttonLayout, 10, 20);
}


void Store::ReturnMain(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        auto cleanUp = CallFunc::create(std::bind(&Store::cleanupNode, this, this));
        
        this->removeAllChildren();
        this->runAction(Sequence::create(FadeOut::create(1),cleanUp,nullptr));
        parent->insertObjects();
    }
}


void Store::GoToNoAds(Ref *pSender, ui::Widget::TouchEventType type)
{
    if(type == ui::Widget::TouchEventType::ENDED)
    {
        log("TOUCH_EVENT_ENDED_IAP");
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        iOSHelper::buyNoAds();
        
        bool adsRemoval = iOSHelper::adsRemoval();
        
        if (!adsRemoval){
            iOSHelper::showAdmobBanner();
        }else{
            iOSHelper::hideAdmobBanner();
        }
        
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        
#endif
        
    }
    
}


void Store::GoToRestoreIap(Ref *pSender, ui::Widget::TouchEventType type)
{
    switch (type)
    {
        case ui::Widget::TouchEventType::BEGAN:
            //label->setText("UIButton Click.");
            log("TOUCH_EVENT_BEGAN");
            break;
        case ui::Widget::TouchEventType::MOVED:
            log("TOUCH_EVENT_MOVED");
            break;
        case ui::Widget::TouchEventType::CANCELED:
            log("TOUCH_EVENT_CANCELED");
            break;
        default:
            // TODO
            break;
        case ui::Widget::TouchEventType::ENDED:
            log("TOUCH_EVENT_ENDED_IAP");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            iOSHelper::restoreIAP();
            
            bool adsRemoval = iOSHelper::adsRemoval();
            if (adsRemoval){
                iOSHelper::hideAdmobBanner();
                MessageBox("Ads Removal restored.", "Alert");
            }
            //#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#endif
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
            break;
            
    }
}

void Store::GoToColor(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    switch (type)
    {
        case ui::Widget::TouchEventType::BEGAN:
            //label->setText("UIButton Click.");
            log("TOUCH_EVENT_BEGAN");
            break;
        case ui::Widget::TouchEventType::MOVED:
            log("TOUCH_EVENT_MOVED");
            break;
        case ui::Widget::TouchEventType::ENDED:
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
            auto colorLayer = new ColorPickerLayer();
            colorLayer->setOpacity(0);
            this->removeAllChildren();
            this->addChild(colorLayer, 100);
            colorLayer->init(this);
            break;
        }
        case ui::Widget::TouchEventType::CANCELED:
            log("TOUCH_EVENT_CANCELED");
            break;
        default:
            // TODO
            break;
    }
    
}

void Store::GoToShapes(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    switch (type)
    {
        case ui::Widget::TouchEventType::BEGAN:
            //label->setText("UIButton Click.");
            log("TOUCH_EVENT_BEGAN");
            break;
        case ui::Widget::TouchEventType::MOVED:
            log("TOUCH_EVENT_MOVED");
            break;
        case ui::Widget::TouchEventType::ENDED:
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
            auto shapesLayer = new ShapesPicker();
            this->removeAllChildren();
            this->addChild(shapesLayer, 100);
            shapesLayer->init(this);
            break;
        }
        case ui::Widget::TouchEventType::CANCELED:
            log("TOUCH_EVENT_CANCELED");
            break;
        default:
            // TODO
            break;
    }
    
}

