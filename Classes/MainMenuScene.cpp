#include "MainMenuScene.h"
#include "GameScene.h"
#include "StoreLayer.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

#if   (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "iOSHelper.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AdmobHelper.h"
#endif

USING_NS_CC;

bool firstTime;

MainMenu::MainMenu()
{
}

MainMenu::~MainMenu()
{
}

Scene* MainMenu::createScene(bool isFirst)
{
    firstTime = isFirst;
	auto scene = Scene::create();
	
	auto layer = MainMenu::create();
	
	scene->addChild(layer);
	return scene;
}

bool MainMenu::init()
{
    //Director::getInstance()->getTextureCache()->removeUnusedTextures();
    
    if(!firstTime){
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                iOSHelper::initInterstitial();
        #elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
                AdmobHelper::requestInterstitial();
        #endif
    }

    
    this->scheduleOnce(schedule_selector(MainMenu::popIntersticial), DISPLAY_TIME_INTERSTITIAL_ADD);
    
	if (!LayerColor::initWithColor(Color4B(225, 225, 225, 255)))
	{
		return false;
	}

//    //gets selected hero
//    UserDefault *def = UserDefault::getInstance();
//    selectedHero = def->getIntegerForKey("HERO", 0);
//    unlockedHeroes = def->getIntegerForKey("UHERO", 0);
//    

    this->insertObjects();
  
    return true;
}

void MainMenu::insertObjects(){
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    UserDefault *def = UserDefault::getInstance();
    
    int R = def->getIntegerForKey("RED", 57);
    int G = def->getIntegerForKey("GREEN", 150);
    int B = def->getIntegerForKey("BLUE", 219);
    
    Color3B color = Color3B(R,G,B);
    
    
    /////////Main Menu buttons
    
    auto auxButtonSprite = Sprite::create("small_button.png");
    
    auto layout = ui::Layout::create();
    //layout->cocos2d::Node::setPosition(origin + Vec2(visibleSize.width / 2, visibleSize.height / 2));
    
    Button::Button("PLAY", color, layout, origin+ Vec2(visibleSize.width / 2,visibleSize.height/ 1.8),  0.8, CC_CALLBACK_2(MainMenu::GoToGame, this), NORMAL, "Aller_Bd.ttf", 42, 255);
    
    
    muteButton = new Button("m", color, layout, origin+ visibleSize/2 - Vec2(auxButtonSprite->getContentSize()),  0.8, CC_CALLBACK_2(MainMenu::Mute, this), SQUARE,"heydings_icons.ttf", 42, 100);
    
    if (CocosDenshion::SimpleAudioEngine::getInstance()->getEffectsVolume()==0){
        muteButton->changeText("Q", "heydings_controls.ttf");
    }
    
    Button::Button("S", color, layout, origin+ visibleSize/2 - Vec2(0,auxButtonSprite->getContentSize().height) , 0.8,CC_CALLBACK_2(MainMenu::GoToStore, this), SQUARE,"heydings_icons.ttf", 42, 100);
    
    Button::Button("s", color, layout, origin+ visibleSize/2 + Vec2(auxButtonSprite->getContentSize().width,-auxButtonSprite->getContentSize().height) , 0.8, CC_CALLBACK_2(MainMenu::GoToRate, this), SQUARE, "heydings_icons.ttf", 42, 200);
    
    
    this->addChild(layout,10,10);
    
    //title
    float titlePositionY = (visibleSize.height - visibleSize.height/5 );
    
    auto label = Label::createWithTTF("JET CAVE", "Aller_Bd.ttf", 52);
    //label->setAlignment(TextHAlignment::CENTER);
    label->setPosition(origin + Vec2(visibleSize.width/2, titlePositionY));
    label->setTextColor(Color4B(color));
    
    ParticleSystemQuad *title_emitter1 = ParticleSystemQuad::create ("hero_particle.plist");
    title_emitter1->setAnchorPoint(Point(0,0));
    title_emitter1->setPosition(origin + Vec2(visibleSize.width/2-label->getContentSize().width/1.5, titlePositionY));
    title_emitter1->cocos2d::Node::setScale(0.2);
    title_emitter1->setEndColor(Color4F(color));
    title_emitter1->setStartColor(Color4F(color));
    title_emitter1->setGravity(-title_emitter1->getGravity());
    
    ParticleSystemQuad *title_emitter2 = ParticleSystemQuad::create ("hero_particle.plist");
    title_emitter2->setAnchorPoint(Point(0,0));
    title_emitter2->setPosition(origin + Vec2(visibleSize.width/2+label->getContentSize().width/1.5, titlePositionY));
    title_emitter2->cocos2d::Node::setScale(0.2);
    title_emitter2->setEndColor(Color4F(color));
    title_emitter2->setStartColor(Color4F(color));
    title_emitter2->setAngle(0);
    
    int heroN = UserDefault::getInstance()->getIntegerForKey("HERO");
    
    std::string heroFileName;
    
    switch(heroN){
        case 0:
            heroFileName = "hero_square.png";
            break;
        case 1:
            heroFileName = "hero_circle.png";
            break;
        case 2:
            heroFileName = "hero_triangle.png";
            break;
        case 3:
            heroFileName = "hero_star.png";
            break;
        case 4:
            heroFileName = "hero_flake.png";
            break;
        case 5:
            heroFileName = "hero_shuriken.png";
            break;
    }
    
    auto heroRect = new Rect(0,  0, HERO_SIZE, HERO_SIZE);
    
    title_emitter1->setDisplayFrame(SpriteFrame::create(heroFileName, *heroRect));
    title_emitter2->setDisplayFrame(SpriteFrame::create(heroFileName, *heroRect));
    
    
    this->addChild(label, 1);
    this->addChild(title_emitter1, 1);
    this->addChild(title_emitter2, 1);
}

void MainMenu::popIntersticial(float dt)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    bool adsRemoval = iOSHelper::adsRemoval();
    
    if (!adsRemoval){
        iOSHelper::showInterstitial();
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    AdmobHelper::showInterstitial();
#endif
}

void MainMenu::GoToGame(Ref *pSender, ui::Widget::TouchEventType type)
{
	switch (type)
	{
	case ui::Widget::TouchEventType::BEGAN:
		//label->setText("UIButton Click.");
		log("TOUCH_EVENT_BEGAN");
		break;
	case ui::Widget::TouchEventType::MOVED:
		log("TOUCH_EVENT_MOVED");
		break;
	
	case ui::Widget::TouchEventType::CANCELED:
		log("TOUCH_EVENT_CANCELED");
		break;
    case ui::Widget::TouchEventType::ENDED:
		log("TOUCH_EVENT_ENDED");
            auto scene = Game::createScene();
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
            Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
		break;
    }
	
}

void MainMenu::GoToStore(Ref *pSender, ui::Widget::TouchEventType type)
{
    switch (type)
    {
        case ui::Widget::TouchEventType::BEGAN:
            //label->setText("UIButton Click.");
            log("TOUCH_EVENT_BEGAN");
            break;
        case ui::Widget::TouchEventType::MOVED:
            log("TOUCH_EVENT_MOVED");
            break;
        case ui::Widget::TouchEventType::CANCELED:
            log("TOUCH_EVENT_CANCELED");
            break;
        default:
            // TODO
            break;
        case ui::Widget::TouchEventType::ENDED:
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
            auto storeLayer = new Store();
            auto init = CallFunc::create([=]()->void{
                storeLayer->init(score, this);
            });
            this->removeAllChildren();
            //this->getChildByTag(10)->setTouchEnabled(false);
            this->addChild(storeLayer, 100);
            storeLayer->runAction(Sequence::create(FadeIn::create(0.2),init,nullptr));
            break;
            
    }
}

void MainMenu::GoToRate(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    switch (type)
    {
        case ui::Widget::TouchEventType::BEGAN:
            //label->setText("UIButton Click.");
            log("TOUCH_EVENT_BEGAN");
            break;
        case ui::Widget::TouchEventType::MOVED:
            log("TOUCH_EVENT_MOVED");
            break;
        case ui::Widget::TouchEventType::ENDED:
            log("TOUCH_EVENT_ENDED");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            iOSHelper::rate();
#endif
            break;
        case ui::Widget::TouchEventType::CANCELED:
            log("TOUCH_EVENT_CANCELED");
            break;
        default:
            // TODO
            break;
    }
    
}


void MainMenu::Mute(Ref *pSender, ui::Widget::TouchEventType type)
{
    
    switch (type)
    {
        case ui::Widget::TouchEventType::BEGAN:
            //label->setText("UIButton Click.");
            log("TOUCH_EVENT_BEGAN");
            break;
        case ui::Widget::TouchEventType::MOVED:
            log("TOUCH_EVENT_MOVED");
            break;
        case ui::Widget::TouchEventType::ENDED:
            log("TOUCH_EVENT_ENDED");
            
            if (CocosDenshion::SimpleAudioEngine::getInstance()->getEffectsVolume()>0){
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
                muteButton->changeText("Q", "heydings_controls.ttf");
                CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(0.0f);
                CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.0f);
            }else{
                muteButton->changeText("m", "heydings_icons.ttf");
                CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0f);
                CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.1f);
            }
            
            break;
        case ui::Widget::TouchEventType::CANCELED:
            log("TOUCH_EVENT_CANCELED");
            break;
        default:
            // TODO
            break;
    }
    
}

