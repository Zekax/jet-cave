#include "Button.h"
//#include "SimpleAudioEngine.h"



USING_NS_CC;

Button::Button(const char* text, Color3B color, ui::Layout *layout, Vec2 position, float scale, ui::Widget::ccWidgetTouchCallback Callback, BUTTON_TYPE buttonType, const std::string font, int fontSize, GLubyte opacity)
{
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
		
	playButton = ui::Button::create();
    
    playButton->setCascadeOpacityEnabled(false);
    playButton->setCascadeColorEnabled(false);
    playButton->setTitleFontName(font);
    
    m_layout = layout;
    m_color = color;
    
    switch (buttonType){
        case NORMAL:
            playButton->loadTextures("Button_f.png","Button_f.png","");
            playButton->setColor(color);
            //playButton->setTitleColor(color);
            playButton->setZoomScale(0.1);
            playButton->setPressedActionEnabled(true);
            break;

        case BUY:{
            playButton->loadTextures("Button_f.png","Button_f.png","");
            playButton->setColor(color);
            //playButton->setTitleColor(color);
            playButton->setZoomScale(0.1);
            playButton->setPressedActionEnabled(true);
            
            auto unlockStar = Sprite::create("hexagon.png");
            unlockStar->setColor(Color3B(245,245,50));
            unlockStar->setScale(0.5);
            playButton->addChild(unlockStar);
            unlockStar->setPosition(40,playButton->getContentSize().height/2);
    
            break;
        }
            
        case SMALL_CIRCLE:
            playButton->loadTextures("small_circle.png","small_circle.png","");
            playButton->setColor(color);
            //playButton->setTitleColor(color);
            playButton->setZoomScale(0.1);
            playButton->setPressedActionEnabled(true);
            
            break;
        
        case SQUARE:
            playButton->loadTextures("small_button.png","small_button.png","");
            playButton->setColor(color);
            //playButton->setTitleColor(color);
            playButton->setZoomScale(0.1);
            playButton->setPressedActionEnabled(true);
            
            break;
            
        case BIGSQUARE:
            playButton->loadTextures("big_round_square.png","big_round_square.png","");
            //playButton->setColor(color);
            //playButton->setTitleColor(Color3B::WHITE);
            
            //playButton->addChild(unlockStar);
            
            break;
            
        case IAP:
        {
            
            playButton->loadTextures("small_circle.png","small_circle.png","");
            //playButton->setBackgroundColor(color, opacity);
            
            auto iapSprite = Sprite::create("IAP_Button.png");
            iapSprite->setScale(0.7);
            iapSprite->setPosition(playButton->getContentSize()/2);
            playButton->setZoomScale(0.1);
            playButton->setPressedActionEnabled(true);
            playButton->addChild(iapSprite);
            
            break;
        }

        case ARROW:
            playButton->loadTextures("white_arrow.png","white_arrow.png","");
            playButton->setTitleColor(Color3B::BLACK);
            //playButton->setBackgroundColor(color, opacity);
            
            break;
        case ICON:
            playButton->setTitleColor(color);
            //playButton->setBackgroundColor(color, 0);
            break;
            
        case SHARE:
            playButton->loadTextures("share.png","share.png","");
            break;
            
        default:
            playButton->loadTextures("round_square.png","round_square.png","");
            break;


            
    }
    if(buttonType!=NORMAL && buttonType!=SQUARE ){
        playButton->setTitleColor(Color3B::BLACK);
        playButton->setColor(color);
        //playButton->setBackgroundColor(color, opacity);
        playButton->setZoomScale(0.1);
        playButton->setPressedActionEnabled(true);
    }

    playButton->setAnchorPoint(Vec2(0.5,0.5));
    playButton->setScale(scale);
	playButton->setTouchEnabled(true);
    
	playButton->setTitleText(text);
	
	playButton->setTitleFontSize(fontSize);
    playButton->setPosition(position);
	playButton->addTouchEventListener(Callback);
	
	layout->addChild(playButton,0,1);

}
Button::~Button(){
    //log("removing button");
	
}

void Button::setShapeButton(std::string shapeUnlockedPath, std::string shapeSelectedPath){
    
    shapeButtonUnlocked = shapeUnlockedPath;
    shapeButtonSelected = shapeSelectedPath;
    
}

void Button::setUnlocked(){
    
    playButton->removeAllChildren();
    auto unlockSprite = Sprite::create(shapeButtonUnlocked);
    unlockSprite->setColor(m_color);
    unlockSprite->setPosition(playButton->getContentSize()/2);
    playButton->addChild(unlockSprite);
    playButton->setTitleText("");
}

void Button::setSelected(){
    
    playButton->removeAllChildren();
    auto selectSprite = Sprite::create(shapeButtonSelected);
    selectSprite->setColor(m_color);
    selectSprite->setPosition(playButton->getContentSize()/2);
    playButton->addChild(selectSprite);
    playButton->setTitleText("");
}

void Button::bounce(){
    auto move = MoveBy::create(0.5, Vec2(0,5));
    playButton->runAction(RepeatForever::create(Sequence::create(move,move->reverse(),nullptr)));
}

void Button::changeText(const char* text, std::string font){
    playButton->setTitleFontName(font);
    playButton->setTitleText(text);
}

void Button::changeColor(Color3B color){
    playButton->setColor(color);
    //playButton->update(0);
}

void Button::flip(){
    playButton->setFlippedX(true);
}

void Button::hide(){
    playButton->setVisible(false);
    playButton->setEnabled(false);
}

void Button::show(){
    playButton->setVisible(true);
    playButton->setEnabled(true);
}

void Button::enable(){
    playButton->setEnabled(true);
}

int Button::getSizeY(){
    return playButton->getBoundingBox().size.width;
}


void Button::moveToX(int x){
    //Size visibleSize = Director::getInstance()->getVisibleSize();
    //playButton->runAction(MoveTo::create(WALL_MOVEMENT_SPEED * visibleSize.width*0.75, Vec2(x,playButton->getPositionY())));
}

void Button::setPositionX(int x){
    playButton->setPositionX(x);
}


