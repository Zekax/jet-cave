#include "Hero.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Hero::Hero(Layer *gameScene,int selectedhero)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    UserDefault *def = UserDefault::getInstance();
    int R = def->getIntegerForKey("RED", 57);
    int G = def->getIntegerForKey("GREEN", 150);
    int B = def->getIntegerForKey("BLUE", 219);
    
    color = Color3B(R,G,B);
    
    int heroN = UserDefault::getInstance()->getIntegerForKey("HERO");
    
    PhysicsBody *bodyHero;
    
    float heroSize = HERO_SIZE/1.3;
    
    switch(heroN){
        case 0:
            heroFileName = "hero_square.png";
            bodyHero = PhysicsBody::createBox(Size(HERO_SIZE,HERO_SIZE), PhysicsMaterial(0.0f, 0.0f, 0.0f),Point(0,0));
            break;
        case 1:
            heroFileName = "hero_circle.png";
            bodyHero = PhysicsBody::createCircle(heroSize, PhysicsMaterial(0.0f, 0.0f, 0.0f),Point(0,0));
            break;
        case 2:
        {
            heroFileName = "hero_triangle.png";
            Vec2 triangle[3] = {Vec2(-heroSize,-heroSize),Vec2(-heroSize,heroSize),Vec2(heroSize,0)};
            bodyHero = PhysicsBody::createPolygon(triangle, 3, PhysicsMaterial(0.0f, 0.0f, 0.0f),Point(0,0));
            break;
        }
        case 3:
            heroFileName = "hero_star.png";
            bodyHero = PhysicsBody::createCircle(heroSize, PhysicsMaterial(0.0f, 0.0f, 0.0f),Point(0,0));
            break;
        case 4:
            heroFileName = "hero_flake.png";
            bodyHero = PhysicsBody::createCircle(heroSize, PhysicsMaterial(0.0f, 0.0f, 0.0f),Point(0,0));
            break;
        case 5:
            heroFileName = "hero_shuriken.png";
            bodyHero = PhysicsBody::createCircle(heroSize, PhysicsMaterial(0.0f, 0.0f, 0.0f),Point(0,0));
            break;
    }
    
    hero = Sprite::create(heroFileName);
    hero->setAnchorPoint(Vec2(0.5,0.5));
    hero->setPosition(Vec2(0,0));
    hero->setColor(color);
    if(heroN>2)
        hero->runAction(RepeatForever::create(RotateBy::create(1, 90)));
    
    bodyHero->setRotationEnable(false);
    bodyHero->setDynamic(true);
    bodyHero->setCollisionBitmask(HERO_COLLISION_BITMASK);
    bodyHero->setContactTestBitmask(true);
    this->setPhysicsBody(bodyHero);

    this->addChild(hero, 1,1);
    this->setPosition(origin + Point(visibleSize.width / 4, visibleSize.height /2));
    this->retain();
    
    heroRect = new Rect(0,  0, HERO_SIZE, HERO_SIZE);
    
    jet_emitter = ParticleSystemQuad::create ("hero_particle.plist");
    jet_emitter->setAnchorPoint(Point(0,0));
    jet_emitter->setPosition(0,0);
    jet_emitter->setEndColor(Color4F(color));
    jet_emitter->setStartColor(Color4F(color));
    jet_emitter->cocos2d::Node::setScale(0.3);
    
    jet_emitter->setDisplayFrame(SpriteFrame::create(heroFileName, *heroRect));
    this->addChild(jet_emitter);
    
    gameScene->addChild(this, 2,1);
}

Hero::~Hero(){
}

void Hero::fly()
{
    auto fly = MoveBy::create(3, Vec2(0,Director::getInstance()->getVisibleSize().height));
    fly->setTag(22);
    this->stopActionByTag(11);
    this->runAction(fly);
    isFalling = false;

}

void Hero::applyForce(float dt)
{
    if (force < 3){
        force += 1;
    }
    if (!isFalling){
        this->setPositionY(this->getPositionY()+2);
    }else{
        this->setPositionY(this->getPositionY()-2);
    }
}

void Hero::fall()
{
    auto fall = MoveBy::create(3, Vec2(0,-Director::getInstance()->getVisibleSize().height));
    fall->setTag(11);
    this->stopActionByTag(22);
    this->runAction(fall);
    force = 0;
    isFalling = true;
}

void Hero::crash()
{
    jet_emitter->stopSystem();
    
    explosion_emitter = ParticleSystemQuad::create ("explosion_particle.plist");
    explosion_emitter->setAnchorPoint(Point(0,0));
    explosion_emitter->setPosition(0,0);
    explosion_emitter->cocos2d::Node::setScale(0.2);
    explosion_emitter->setEndColor(Color4F(color));
    explosion_emitter->setStartColor(Color4F(color));
    explosion_emitter->setDisplayFrame(SpriteFrame::create(heroFileName, *heroRect));
    
    this->addChild(explosion_emitter);
    hero->setVisible(false);
}

void Hero::scale(float scale){
    jet_emitter->setScale(scale*0.2);
    hero->setScale(scale);
}


