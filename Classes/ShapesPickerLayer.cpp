#include "ShapesPickerLayer.h"
#include "ShapesPickerLayer.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

#if   (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "iOSHelper.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AdmobHelper.h"
#endif

USING_NS_CC;


ShapesPicker::ShapesPicker()
{
    LayerColor::initWithColor(Color4B(225, 225, 225, 2));
    
}


ShapesPicker::~ShapesPicker()
{
    
}

bool ShapesPicker::init(Store *parent)
{
    parentLayer = parent;
    
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    UserDefault *def = UserDefault::getInstance();
    
    starScore = def->getIntegerForKey("STARS", 0);
    
    int R = def->getIntegerForKey("RED", 57);
    int G = def->getIntegerForKey("GREEN", 150);
    int B = def->getIntegerForKey("BLUE", 219);
    
    Color3B color = Color3B(R,G,B);
    
    auto label = Label::createWithTTF("SHAPES", "Aller_Bd.ttf", 32);
    
    label->setPosition(Vec2(origin.x + visibleSize.width / 2,origin.y + visibleSize.height - label->getContentSize().height));
    label->setTextColor(Color4B(color));
    this->addChild(label, 1);
    
    auto buttonLayout = ui::Layout::create();
    buttonLayout->setSize(visibleSize / 2);
    
    std::string auxUnlocked = def->getStringForKey("UNLOCKED_HEROES","200000");
    
    std::copy(auxUnlocked.begin(), auxUnlocked.end(), unlocked);
    
    unlocked[auxUnlocked.size()] = '\0';
   
    shapeButtons[0] = (new  Button("   200", color, buttonLayout, Size(origin.x +visibleSize.width / 2 -180, origin.y +visibleSize.height / 2 + 70), 1, CC_CALLBACK_2(ShapesPicker::SelectSquare, this),BUY, "Aller_Bd.ttf", 32, 200));
    shapeButtons[0]->setShapeButton("Button_square.png", "Button_square_s.png");
    
    shapeButtons[1] = (new  Button("   200", color, buttonLayout, Size(origin.x +visibleSize.width / 2, origin.y +visibleSize.height / 2 + 70), 1, CC_CALLBACK_2(ShapesPicker::SelectCircle, this),    BUY, "Aller_Bd.ttf", 32, 200));
    shapeButtons[1]->setShapeButton("Button_circle.png", "Button_circle_s.png");
    
    shapeButtons[2] = (new  Button("   200", color, buttonLayout, Size(origin.x +visibleSize.width / 2 +180, origin.y +visibleSize.height / 2 + 70), 1, CC_CALLBACK_2(ShapesPicker::SelectTriangle, this),BUY, "Aller_Bd.ttf", 32, 200));
    shapeButtons[2]->setShapeButton("Button_triangle.png", "Button_triangle_s.png");
    
    shapeButtons[3] = (new  Button("   200", color, buttonLayout, Size(origin.x +visibleSize.width / 2 -180, origin.y +visibleSize.height / 2 - 60), 1, CC_CALLBACK_2(ShapesPicker::SelectStar, this),BUY, "Aller_Bd.ttf", 32, 200));
    shapeButtons[3]->setShapeButton("Button_star.png", "Button_star_s.png");
    
    shapeButtons[4] = (new  Button("   200", color, buttonLayout, Size(origin.x +visibleSize.width / 2 , origin.y +visibleSize.height / 2 - 60), 1, CC_CALLBACK_2(ShapesPicker::SelectSFlake, this),    BUY, "Aller_Bd.ttf", 32, 200));
    shapeButtons[4]->setShapeButton("Button_flake.png", "Button_flake_s.png");
    
    shapeButtons[5] = (new  Button("   200", color, buttonLayout, Size(origin.x +visibleSize.width / 2 +180, origin.y +visibleSize.height / 2 - 60), 1, CC_CALLBACK_2(ShapesPicker::SelectShuriken, this),BUY, "Aller_Bd.ttf", 32, 200));
    shapeButtons[5]->setShapeButton("Button_shuriken.png", "Button_shuriken_s.png");
    
    this->updateButtons();
        
    Button::Button("", color, buttonLayout, Size(origin.x +visibleSize.width / 2 -325, origin.y +visibleSize.height / 2 + 70), -0.2, CC_CALLBACK_2(ShapesPicker::ReturnToStore, this), ARROW, "Aller_Bd.ttf", 32, 200);
    
    this->addChild(buttonLayout,10,30);
    
    auto starScoreSprite = Sprite::create("hexagon.png");
    starScoreSprite->setPosition(Vec2(origin.x + visibleSize.width/2 +30, origin.y + visibleSize.height/5-5));
    starScoreSprite->setScale(0.5);
    starScoreSprite->setColor(Color3B::YELLOW);
    this->addChild(starScoreSprite,3);
    
    char starsString[20];
    sprintf(starsString, "%d", starScore);
    
    starsLabel = Label::createWithTTF(starsString, "Aller_Bd.ttf", 24);
    starsLabel->setDimensions(100, 24);
    starsLabel->setAnchorPoint(Vec2(1,0.5));
    starsLabel->setAlignment(TextHAlignment::RIGHT);
    starsLabel->setPosition(Vec2(origin.x + visibleSize.width/2 , origin.y + visibleSize.height/5));
    starsLabel->setColor(Color3B(0, 0, 0));
    this->addChild(starsLabel,3);
    
    return true;
}

void ShapesPicker::updateButtons()
{
    
    for (int i=0;i< 6; i++){
        
        if(unlocked[i]=='1')
            shapeButtons[i]->setUnlocked();
        if(unlocked[i]=='2')
            shapeButtons[i]->setSelected();
    }
    
    UserDefault *def = UserDefault::getInstance();
    
    def->setStringForKey("UNLOCKED_HEROES",unlocked);
    
    
    def->flush();

}

void ShapesPicker::selectButton(int buttonN){
    
    for (int i=0;i< 6; i++){
        if(unlocked[i]=='2')unlocked[i]='1';
    }
    if(unlocked[buttonN]=='1'){
        
        unlocked[buttonN]='2';
        updateButtons();
        UserDefault::getInstance()->setIntegerForKey("HERO",buttonN);
        //buyShape(buttonN);
    }else{
        if(starScore>=200 && unlocked[buttonN]=='0')
        {
            
            buyShape(buttonN);
            updateButtons();
        }
    }
    
}

void ShapesPicker::buyShape(int shapeN){
    
    starScore= starScore-200;
    
    char starsString[20];
    sprintf(starsString, "%d", starScore);
    
    starsLabel->setString(starsString);
    
    //Number of stars collected
    UserDefault::getInstance()->setIntegerForKey("STARS", starScore);
    unlocked[shapeN]='2';
    selectButton(shapeN);
}



void ShapesPicker::ReturnToStore(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        auto cleanUp = CallFunc::create(std::bind(&ShapesPicker::cleanupNode, this, this));
        if(type == ui::Widget::TouchEventType::ENDED){
            parentLayer->insertObjects();
            this->removeAllChildren();
            this->runAction(Sequence::create(FadeOut::create(1),cleanUp,nullptr));
        }
    }
}

void ShapesPicker::SelectSquare(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        selectButton(0);


    }
    
}

void ShapesPicker::SelectCircle(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        selectButton(1);
    }
    
}

void ShapesPicker::SelectTriangle(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        selectButton(2);
    }
    
}

void ShapesPicker::SelectStar(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        selectButton(3);
    }
    
}

void ShapesPicker::SelectSFlake(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        selectButton(4);
    }
    
}

void ShapesPicker::SelectShuriken(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        selectButton(5);
    }
    
}

