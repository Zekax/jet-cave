/*
Copyright (c) 2014 Mudafar
GPLv3
*/


#ifndef  __ADMOB_HELPER_H_
#define  __ADMOB_HELPER_H_

class AdmobHelper
{
public:
	static void hideAd();
	static void showAd();
	static void requestInterstitial();
	static void showInterstitial();
	static void showShare();
	static bool isAdShowing;

};


#endif // __ADMOB_HELPER_H_

