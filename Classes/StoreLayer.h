#ifndef __STORE_LAYER_H__
#define __STORE_LAYER_H__

#include "cocos2d.h"
#include "Button.h"
#include "ui/UILayout.h"
#include "MainMenuScene.h"

class Store : public cocos2d::LayerColor
{
public:
    Store();
    ~Store();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init(int myScore, MainMenu *root);
    void insertObjects();
    
    void ReturnMain(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToNoAds(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToRestoreIap(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToColor(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToShapes(Ref *pSender, ui::Widget::TouchEventType type);
    void cleanupNode(Node *inNode)
    {
        inNode->removeFromParentAndCleanup(true);
        inNode->release();
    }
    
private:
    int stars;
    MainMenu *parent;
};

#endif