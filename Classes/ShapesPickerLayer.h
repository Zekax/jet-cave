#ifndef __SHAPES_LAYER_H__
#define __SHAPES_LAYER_H__

#include "cocos2d.h"
#include "Button.h"
#include "ui/UILayout.h"
#include "StoreLayer.h"

class ShapesPicker : public cocos2d::LayerColor
{
public:
    ShapesPicker();
    ~ShapesPicker();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init(Store *parent);
    
    void insertObjects();
    void updateButtons();
    void selectButton(int buttonN);
    void buyShape(int shapeN);
    
    void ReturnToStore(Ref *pSender, ui::Widget::TouchEventType type);
    void SelectSquare(Ref *pSender, ui::Widget::TouchEventType type);
    void SelectCircle(Ref *pSender, ui::Widget::TouchEventType type);
    void SelectTriangle(Ref *pSender, ui::Widget::TouchEventType type);
    void SelectStar(Ref *pSender, ui::Widget::TouchEventType type);
    void SelectSFlake(Ref *pSender, ui::Widget::TouchEventType type);
    void SelectShuriken(Ref *pSender, ui::Widget::TouchEventType type);

    void cleanupNode(Node *inNode)
    {
        inNode->removeFromParentAndCleanup(true);
        inNode->release();
    }

private:

    unsigned int starScore=0;
    const char *unlockedButtons;
    char unlocked[6];
    Store *parentLayer;
    Label *starsLabel;
    Button* shapeButtons[6];
    
};

#endif