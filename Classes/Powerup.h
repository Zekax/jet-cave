#pragma once
#ifndef __POWERUP_H__
#define __POWERUP_H__

#include "cocos2d.h"

USING_NS_CC;

class Powerup: public cocos2d::Node
{
private:

    int powerType;
	Size visibleSize;
    cocos2d::ParticleSystemQuad* powerup_emitter;
    
    std::string powerFiles[3] = {
        "spiral_particle_up.plist",
        "spiral_particle_down.plist",
        "spiral_particle_unk.plist"
    };

public:

	Powerup(Layer *gameScene, Vec2 position, int powerType, int WALL_GAP);
	void cleanupSprite(Node *inNode);
	~Powerup();
    int getType();

};

#endif