#include "Star.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Star::Star(Layer *gameScene, Vec2 position, int vertPos, int WALL_GAP, Color3B STAR_COLOR)
{
	visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
    //log("star!");
    //this->setAnchorPoint(Vec2(1,1));
    
    //this->create("hexagon.png");
    
    initWithFile("hexagon.png");
    
    int distance = HERO_SIZE * WALL_GAP/2 - this->getContentSize().height/2;
    
    if (vertPos==0)position.y = position.y - distance;
    else if (vertPos==2)position.y = position.y + distance;
    
    this->setPosition(position.x, position.y);
    this->setScale(0.3);
    this->setColor(STAR_COLOR);
    
    auto bodyStar = PhysicsBody::createCircle(this->getContentSize().width/2*this->getScale(), PhysicsMaterial(0.0f, 0.0f, 0.0f));
    bodyStar->setCollisionBitmask(false);
    bodyStar->setContactTestBitmask(STAR_COLLISION_BITMASK);
    bodyStar->setDynamic(false);
    this->setPhysicsBody(bodyStar);
    
    auto rotate = RotateBy::create(1,360);
    
    this->runAction(RepeatForever::create(Sequence::create(rotate,DelayTime::create(1),rotate->reverse(),DelayTime::create(1),nullptr)));
    
    //this->addChild(starSprite);
    
    gameScene->addChild(this,10);
    
}

void Star::cleanupSprite(Node *inNode)
{
	// call your destroy particles here
	// remove the sprite
	inNode->removeFromParentAndCleanup(true);
	inNode->release();
	log("sprite removed!");
	
	
}

Star::~Star()
{
}
