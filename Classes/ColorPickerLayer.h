#ifndef __GAMEOVER_SCENE_H__
#define __GAMEOVER_SCENE_H__

#include "cocos2d.h"
#include "Button.h"
#include "ui/UILayout.h"
#include "cocos-ext.h"
#include "StoreLayer.h"

class ColorPickerLayer : public cocos2d::LayerColor
{
public:
    ColorPickerLayer();
    ~ColorPickerLayer();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init(Store *parent);
    
    void insertObjects();
    
    void valueChanged(Ref *pSender, extension::Control::EventType type);
    void setColor(Ref *pSender, ui::Widget::TouchEventType type);
    void ReturnToStore(Ref *pSender, ui::Widget::TouchEventType type);
    void cleanupNode(Node *inNode)
    {
        inNode->removeFromParentAndCleanup(true);
        inNode->release();
    }
    
    
private:
    unsigned int starScore=0;
    Label *starsLabel;
    Label *label;
    Store *parentLayer;
    Sprite *box;
    Color3B color;
    Button *buyColorButton;
    Button *returnButton;
    extension::ControlColourPicker *colourPicker;
};
#endif