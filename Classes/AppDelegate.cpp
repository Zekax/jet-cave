#include "AppDelegate.h"
#include "MainMenuScene.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

#if   (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    #include "iOSHelper.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    #include "AdmobHelper.h"
#endif

//#include "AppController.h"
USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
		glview = GLViewImpl::create("Jet Cave");
		//glview = GLViewImpl::createWithRect("Amazing Thief", Rect(0, 0, 540, 760), 1);
		//glview = GLViewImpl::createWithFullScreen("Amazing Thief");
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    //director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

	//auto fileUtils = FileUtils::getInstance();
	auto screenSize = glview->getFrameSize();
    //log("screen height: %f",screenSize.height);
	// 2. add search path for images
	std::vector<std::string> searchPaths;
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if (2048 == screenSize.width || 2048 == screenSize.height)// retina iPad
    {
        searchPaths.push_back("HDR");
        searchPaths.push_back("HD");
        searchPaths.push_back("SD");
        if (true == IS_LANDSCAPE)
        {
            glview->setDesignResolutionSize(1024, 768, ResolutionPolicy::NO_BORDER);
        }
        else
        {
            glview->setDesignResolutionSize(768, 1024, ResolutionPolicy::NO_BORDER);
        }
    }
    
    else if (1024 == screenSize.width || 1024 == screenSize.height)// non retina iPad
    {
        searchPaths.push_back("HDR");
        searchPaths.push_back("HD");
        searchPaths.push_back("SD");
        
        if (true == IS_LANDSCAPE)
        {
            glview->setDesignResolutionSize(1024, 768, ResolutionPolicy::NO_BORDER);
        }
        else
        {
            glview->setDesignResolutionSize(768, 1024, ResolutionPolicy::NO_BORDER);
        }
    }
    else if (1136 == screenSize.width || 1136 == screenSize.height)// retina iPhone (5 and 5S)
    {
        searchPaths.push_back("HDR");
        searchPaths.push_back("HD");
        searchPaths.push_back("SD");
        if (true == IS_LANDSCAPE)
        {
            glview->setDesignResolutionSize(1136, 640, ResolutionPolicy::NO_BORDER);
        }
        else
        {
            //glview->setFrameSize(640, 1136);
            glview->setDesignResolutionSize(640, 1136, ResolutionPolicy::NO_BORDER);
        }
    }
    else if (960 == screenSize.width || 960 == screenSize.height)// retina iPhone (4 and 4S)
    {
        searchPaths.push_back("HDR");
        if (true == IS_LANDSCAPE)
        {
            glview->setDesignResolutionSize(960, 640, ResolutionPolicy::NO_BORDER);
        }
        else
        {
            glview->setDesignResolutionSize(640, 960, ResolutionPolicy::NO_BORDER);
        }
    }
    else if (1334 == screenSize.width || 1334 == screenSize.height)// retina iPhone (6)
    {
        searchPaths.push_back("HDR");
        searchPaths.push_back("HD");
        searchPaths.push_back("SD");
        if (true == IS_LANDSCAPE)
        {
            glview->setDesignResolutionSize(1334, 750, ResolutionPolicy::NO_BORDER);
        }
        else
        {
            glview->setDesignResolutionSize(750, 1334, ResolutionPolicy::NO_BORDER);
        }
    }
    else
    {
        if (screenSize.width >= 1080 || 1080 <= screenSize.height)// android devices that have a high resolution and iPhone 6 Plus
        {
            searchPaths.push_back("HDR");
            searchPaths.push_back("HD");
            searchPaths.push_back("SD");
            if (true == IS_LANDSCAPE)
            {
                glview->setDesignResolutionSize(1280, 720, ResolutionPolicy::NO_BORDER);
            }
            else
            {
                glview->setDesignResolutionSize(720, 1280, ResolutionPolicy::NO_BORDER);
            }
        }
        else
        {
            searchPaths.push_back("HDR");
            if (true == IS_LANDSCAPE)
            {
                glview->setDesignResolutionSize(480, 320, ResolutionPolicy::NO_BORDER);
            }
            else
            {
                glview->setDesignResolutionSize(320, 480, ResolutionPolicy::NO_BORDER);
            }
        }
    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    log("ads removal: %i",iOSHelper::adsRemoval());
    
    bool adsRemoval = iOSHelper::adsRemoval();
    
    if (!adsRemoval){
        iOSHelper::showAdmobBanner();
    }else{
        iOSHelper::hideAdmobBanner();
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if (visible )//&& !AdmobHelper::isAdShowing
    {
        AdmobHelper::showAd();
    }else{
        AdmobHelper::hideAd();
    }
#endif
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    //searchPaths.push_back("HDR");
    //searchPaths.push_back("HD");
    searchPaths.push_back("SD");
    if (true == IS_LANDSCAPE)
    {
        glview->setFrameSize(800, 600);
        glview->setDesignResolutionSize(800, 600, ResolutionPolicy::NO_BORDER);
    }
    else
    {
        glview->setFrameSize(600, 800);
        glview->setDesignResolutionSize(600, 800, ResolutionPolicy::NO_BORDER);
    }
    
#endif
    searchPaths.push_back("audio");
    searchPaths.push_back("fonts");
    searchPaths.push_back("Particles");
    searchPaths.push_back("extensions");
    
    FileUtils::getInstance()->setSearchPaths(searchPaths);
    
//    UserDefault *def = UserDefault::getInstance();
//    auto hero = def->getIntegerForKey("HERO", 0);
//    std::string heroSpriteFramesFile = "ninja" + std::to_string(hero);
    
    //log(heroSpripeFramesFile);
    
//    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(heroSpriteFramesFile+".plist", heroSpriteFramesFile+".pvr.ccz");
    
//    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Stick Ninja_Background_1.wav");
//    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Stick Ninja_Background_2.wav");
//    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Stick Ninja_Background_3.wav");
//    CocosDenshion::SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Stick Ninja_Background_4.wav");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("UITap.wav");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("GrabCoin.wav");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("PowerDown.wav");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("PowerUp.wav");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("Crash.wav");
//    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("FlipStick.wav");
//    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("PerfectLanding.wav");
//    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("StarCatch.wav");
//    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("StickKick.wav");
    
    // create a scene. it's an autorelease object
    auto scene = MainMenu::createScene(true);
    
    // run
    director->runWithScene(scene);
    
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();
    
    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();
    
    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
