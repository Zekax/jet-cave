#include "GameOverLayer.h"
#include "GameScene.h"
#include "MainMenuScene.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

#if   (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "iOSHelper.h"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AdmobHelper.h"
#endif

USING_NS_CC;


GameOver::GameOver()
{
    LayerColor::initWithColor(Color4B(225, 225, 225, 2));
    
}


GameOver::~GameOver()
{
    
}

bool GameOver::init(int myScore)
{
    score = myScore;
    
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    UserDefault *def = UserDefault::getInstance();
    
    int R = def->getIntegerForKey("RED", 57);
    int G = def->getIntegerForKey("GREEN", 150);
    int B = def->getIntegerForKey("BLUE", 219);
    
    Color3B color = Color3B(R,G,B);
    
    auto label = Label::createWithTTF("GAME OVER", "Aller_Bd.ttf", 32);
    
    label->setPosition(Vec2(origin.x + visibleSize.width / 2,origin.y + visibleSize.height - label->getContentSize().height));
    label->setTextColor(Color4B(color));
    this->addChild(label, 1);
    
    //Get and set highscore
    auto highScore = def->getIntegerForKey("HIGHSCORE", 0);
    if (score > highScore)
    {
        highScore = score;
        def->setIntegerForKey("HIGHSCORE", highScore);
        
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    iOSHelper::reportScore(score);
#endif
        
    }
    
    //Get and set number of times user lost in a row
    auto lostGame = def->getIntegerForKey("LOST", 0);
    lostGame +=1;
    if (lostGame >= 5)
    {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        iOSHelper::initInterstitial();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        AdmobHelper::requestInterstitial();
#endif
        
        this->scheduleOnce(schedule_selector(GameOver::popIntersticial), DISPLAY_TIME_INTERSTITIAL_ADD);
        def->setIntegerForKey("LOST", 0);
    }else{
        def->setIntegerForKey("LOST", lostGame);
    }
    
    //flush all user variables for now
    def->flush();
    
    auto sLabel = Label::createWithTTF("SCORE", "Aller_Bd.ttf", 22);
    sLabel->setPosition(Point(origin.x +visibleSize.width * 0.40 , origin.y +visibleSize.height - visibleSize.height / 6));
    sLabel->setAlignment(TextHAlignment::LEFT);
    sLabel->setAnchorPoint(Point(0.5f, 1.0f));
    sLabel->setTextColor(Color4B(0, 0, 0, 255));
    
    __String *tempScore = __String::createWithFormat("%i", score);
    auto currentScore = LabelTTF::create(tempScore->getCString(), "Aller_Bd.ttf", 32);
    currentScore->setPosition(Point(origin.x +visibleSize.width * 0.40 , origin.y +visibleSize.height - visibleSize.height / 6 - sLabel->getContentSize().height -50));
    currentScore->setColor(Color3B(0, 0, 0));
    currentScore->setAnchorPoint(Point(0.5f, 0.0f));
    this->addChild(sLabel);
    this->addChild(currentScore);
    //this->addChild(scoreLayout);
    
    auto hsLabel = Label::createWithTTF("BEST", "Aller_Bd.ttf", 22);
    hsLabel->setPosition(Point(origin.x +visibleSize.width * 0.60 , origin.y +visibleSize.height - visibleSize.height / 6));
    hsLabel->setAlignment(TextHAlignment::LEFT);
    hsLabel->setAnchorPoint(Point(0.5f, 1.0f));
    hsLabel->setColor(Color3B(0, 0, 0));
    this->addChild(hsLabel);
    
    __String *tempHighScore = __String::createWithFormat("%i", highScore);
    auto highScoreLabel = LabelTTF::create(tempHighScore->getCString(), "Aller_Bd.ttf", 32);
    highScoreLabel->setColor(Color3B::BLACK);
    highScoreLabel->setPosition(Point(origin.x +visibleSize.width * 0.60 , origin.y +visibleSize.height - visibleSize.height / 6 - hsLabel->getContentSize().height -50));
    highScoreLabel->setAnchorPoint(Point(0.5f, 0.0f));
    this->addChild(highScoreLabel);
    
    auto buttonLayout = ui::Layout::create();
    buttonLayout->setSize(visibleSize / 2);
    
    Button::Button("H", color, buttonLayout, Size(origin.x +visibleSize.width / 2 -225, origin.y +visibleSize.height / 2 - 80), 0.8, CC_CALLBACK_2(GameOver::GoToMainMenuScene, this), SQUARE, "heydings_icons.ttf", 42, 200);
    Button::Button("g", color, buttonLayout, Size(origin.x +visibleSize.width / 2 -75, origin.y +visibleSize.height / 2 - 80), 0.8, CC_CALLBACK_2(GameOver::GoToLB, this), SQUARE, "heydings_controls.ttf", 42, 200);
    Button::Button("s", color, buttonLayout, Size(origin.x +visibleSize.width / 2 +75, origin.y +visibleSize.height / 2 - 80), 0.8, CC_CALLBACK_2(GameOver::GoToRate, this), SQUARE, "heydings_icons.ttf", 42, 200);
    Button::Button("r", color, buttonLayout, Size(origin.x +visibleSize.width / 2 +225, origin.y +visibleSize.height / 2 - 80), 0.8, CC_CALLBACK_2(GameOver::GoToGameScene, this), SQUARE, "heydings_controls.ttf", 42, 200);
    Button::Button("", Color3B::WHITE, buttonLayout, Size(origin.x +visibleSize.width / 2, origin.y +visibleSize.height / 2), 0.2, CC_CALLBACK_2(GameOver::GoToShare, this), SHARE, "Aller_Bd.ttf", 42, 200);
    
    this->addChild(buttonLayout);
    
    return true;
}

void GameOver::popIntersticial(float dt)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    bool adsRemoval = iOSHelper::adsRemoval();
    
    if (!adsRemoval){
        iOSHelper::showInterstitial();
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    AdmobHelper::showInterstitial();
#endif

}

void GameOver::GoToGameScene(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        Director::getInstance()->getTextureCache()->removeUnusedTextures();
        auto scene = Game::createScene();
        Director::getInstance()->replaceScene(scene);
    }
}
void GameOver::GoToMainMenuScene(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        auto scene = MainMenu::createScene(false);
        Director::getInstance()->replaceScene(scene);
    }
}
void GameOver::GoToLB(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    
    switch (type)
    {
        case ui::Widget::TouchEventType::BEGAN:
            //label->setText("UIButton Click.");
            log("TOUCH_EVENT_BEGAN");
            break;
        case ui::Widget::TouchEventType::MOVED:
            log("TOUCH_EVENT_MOVED");
            break;
        case ui::Widget::TouchEventType::ENDED:
            log("TOUCH_EVENT_ENDED");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            iOSHelper::showGameCenter();
#endif
            break;
        case ui::Widget::TouchEventType::CANCELED:
            log("TOUCH_EVENT_CANCELED");
            break;
        default:
            // TODO
            break;
    }
}
void GameOver::GoToShare(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    switch (type)
    {
        case ui::Widget::TouchEventType::BEGAN:
            //label->setText("UIButton Click.");
            log("TOUCH_EVENT_BEGAN");
            break;
        case ui::Widget::TouchEventType::MOVED:
            log("TOUCH_EVENT_MOVED");
            break;
        case ui::Widget::TouchEventType::ENDED:
            log("TOUCH_EVENT_ENDED");
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            iOSHelper::share(score);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
            AdmobHelper::showShare();
#endif
            break;
        case ui::Widget::TouchEventType::CANCELED:
            log("TOUCH_EVENT_CANCELED");
            break;
        default:
            // TODO
            break;
    }
    
}

void GameOver::GoToRate(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    switch (type)
    {
        case ui::Widget::TouchEventType::BEGAN:
            //label->setText("UIButton Click.");
            log("TOUCH_EVENT_BEGAN");
            break;
        case ui::Widget::TouchEventType::MOVED:
            log("TOUCH_EVENT_MOVED");
            break;
        case ui::Widget::TouchEventType::ENDED:
            log("TOUCH_EVENT_ENDED");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
            iOSHelper::rate();
#endif
            break;
        case ui::Widget::TouchEventType::CANCELED:
            log("TOUCH_EVENT_CANCELED");
            break;
        default:
            // TODO
            break;
    }
    
}

