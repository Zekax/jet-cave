#ifndef __BUTTON_H__
#define __BUTTON_H__

#pragma once
#include "cocos2d.h"
#include "ui/UIButton.h"
#include "ui/UILayout.h"
#include "Definitions.h"
USING_NS_CC;

class Button{
public:
    Button(const char* text, Color3B color,  ui::Layout *layout, Vec2 position, float scale, ui::Widget::ccWidgetTouchCallback Callback, BUTTON_TYPE buttonType, std::string font, int fontSize, GLubyte opacity);
	~Button();

    void bounce();
    void flip();
    void hide();
    void show();
    void enable();
    int getSizeY();
    void moveToX(int x);
    void setPositionX(int x);
    void changeText(const char* text, std::string font);
    void changeColor(Color3B color);
    void setShapeButton(std::string shapeUnlockedPath, std::string shapeSelectedPath);
    void setUnlocked();
    void setSelected();
    
private:
    Color3B m_color;
    ui::Layout *m_layout;
    ui::Button *playButton;
    std::string shapeButtonUnlocked;
    std::string shapeButtonSelected;
	
};

#endif