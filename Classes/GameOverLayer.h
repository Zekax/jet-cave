#ifndef __GAMEOVER_SCENE_H__
#define __GAMEOVER_SCENE_H__

#include "cocos2d.h"
#include "Button.h"
#include "ui/UILayout.h"

class GameOver : public cocos2d::LayerColor
{
public:
    GameOver();
    ~GameOver();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init(int myScore);
    
    void popIntersticial(float dt);
    void GoToGameScene(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToMainMenuScene(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToLB(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToRate(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToShare(Ref *pSender, ui::Widget::TouchEventType type);
    
private:
    int stars;
    int score;
    
};

#endif