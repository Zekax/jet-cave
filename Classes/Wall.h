#pragma once
#ifndef __Wall_H__
#define __Wall_H__

#include "cocos2d.h"

USING_NS_CC;

class Wall : public cocos2d::Node
{
private:
    
    Texture2D *tex;
    Rect *heroRect;

public:

	Wall(Layer *gameScene, int posY, int backgroundNumber, int WALL_GAP, Color3B WALL_COLOR);
	~Wall();
    
    void cleanupSprite(Sprite *inSprite);
    void setGap(int gap);
	
    Color3B wallColor[5] = {Color3B(230,219,168),
                            Color3B(86,93,99),
                            Color3B(153,120,79),
                            Color3B(117,131,150),
                            Color3B(207,212,232)
                            };
};

#endif