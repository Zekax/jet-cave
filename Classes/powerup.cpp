#include "Powerup.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Powerup::Powerup(Layer *gameScene, Vec2 position, int pType, int WALL_GAP)
{
	visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    powerType = pType;
    
    //log("power!");
    //this->setAnchorPoint(Vec2(1,1));
    
    //this->retain();
    
    auto bodyStar = PhysicsBody::createCircle(10, PhysicsMaterial(0.0f, 0.0f, 0.0f));
    bodyStar->setCollisionBitmask(false);
    bodyStar->setContactTestBitmask(POWER_COLLISION_BITMASK);
    bodyStar->setDynamic(false);
    this->setPhysicsBody(bodyStar);
    
    Sprite *powerSprite = Sprite::create("spiral_particle.png");

    powerup_emitter = ParticleSystemQuad::create (powerFiles[powerType]);
    powerup_emitter->setAnchorPoint(Point(0,0));
    powerup_emitter->setPosition(0,0);
    powerup_emitter->setPositionType(ParticleSystem::PositionType::GROUPED);
    powerup_emitter->setScale(0.6);
    //m_emitter->setGravity(Point(-10,0));
    
    this->addChild(powerup_emitter);
    
    float posY = position.y- HERO_SIZE * WALL_GAP/2 + CCRANDOM_0_1() * HERO_SIZE * WALL_GAP;
    if(posY>position.y)posY=posY-powerSprite->getContentSize().height;
    else posY=posY+powerSprite->getContentSize().height;
    
    this->setPosition(position.x, posY);
    
//    auto up = MoveBy::create(1, Vec2(0, HERO_SIZE * WALL_GAP-powerSprite->getContentSize().height));
//    auto moveup = EaseIn::create(up, 0.5);
//    auto down = MoveBy::create(1, Vec2(0,- (HERO_SIZE * WALL_GAP-powerSprite->getContentSize().height)));
//    auto movedown = EaseIn::create(down, 0.5);
    
//    if(CCRANDOM_0_1()<0.5){
//        this->setPosition(position.x, position.y- HERO_SIZE * WALL_GAP/2 + powerSprite->getContentSize().height/2);
//        this->runAction(RepeatForever::create(Sequence::create(moveup,movedown,nullptr)));
//    }else{
//        this->setPosition(position.x, position.y+ HERO_SIZE * WALL_GAP/2 - powerSprite->getContentSize().height/2);
//        this->runAction(RepeatForever::create(Sequence::create(moveup->reverse(),movedown->reverse(),nullptr)));
//    }
//    
    gameScene->addChild(this,10);
    
}

void Powerup::cleanupSprite(Node *inNode)
{
	// call your destroy particles here
	// remove the sprite
	inNode->removeFromParentAndCleanup(true);
	inNode->release();
	log("sprite removed!");
	
	
}

int Powerup::getType()
{
    return powerType;
}

Powerup::~Powerup()
{
}
