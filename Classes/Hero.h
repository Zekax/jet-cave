#pragma once
#ifndef __HERO_H__
#define __HERO_H__

#include "cocos2d.h"


class Hero : public cocos2d::Node
{
public:
	Hero(cocos2d::Layer *gameScene,int selectedhero);
    ~Hero();
    
    void fly();
    void applyForce(float dt);
    void fall();
    void crash();
    void scale(float scale);

	
private:
    
    int force = 0;
    bool isFalling = false;
    std::string heroFileName;
    
    cocos2d::Sprite *hero;
    cocos2d::Rect *heroRect;
    cocos2d::Color3B color;
    cocos2d::ParticleSystemQuad* jet_emitter;
    cocos2d::ParticleSystemQuad* explosion_emitter;
    
};

#endif // __HERO_SCENE_H__
