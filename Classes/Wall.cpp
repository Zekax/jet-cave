#include "Wall.h"
#include "Definitions.h"

USING_NS_CC;

Wall::Wall(Layer *gameScene, int posY, int backgroundNumber, int WALL_GAP, Color3B WALL_COLOR)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    ////////Wall Block
    Size wallSize;
    wallSize.setSize(visibleSize.width*WALL_WIDTH,visibleSize.height*WALL_HEIGHT);
    heroRect = new Rect(0,  0, wallSize.width, wallSize.height);
    
    Color3B* buffer = (Color3B*)malloc((int)4 * sizeof(Color3B));
    tex = new Texture2D();
    {
        for (int y = 0; y < 2; ++y) {
            
            for (int x = 0; x < 2; ++x) {
                
                int index = y * 2 + x;
                
                buffer[index] = WALL_COLOR;
                
            }
        }
        tex->initWithData(buffer, 4, Texture2D::PixelFormat::RGB888, 2, 2, Size(2,2));
    }
    
    tex->setTexParameters({GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT});
    
    auto topSprite = Sprite::createWithTexture(tex, *heroRect);
    auto bottomSprite = Sprite::createWithTexture(tex, *heroRect);
    
    auto TopBody = PhysicsBody::createBox(Size(wallSize.width,1), PhysicsMaterial(0.0f, 0.0f, 0.0f),Point(0,-wallSize.height/2));
    TopBody->setDynamic(false);
    TopBody->setCollisionBitmask(WALL_COLLISION_BITMASK);
    TopBody->setContactTestBitmask(true);
    topSprite->setPhysicsBody(TopBody);
    
    auto bottomBody = PhysicsBody::createBox(Size(wallSize.width,1), PhysicsMaterial(0.0f, 0.0f, 0.0f),Point(0,wallSize.height/2));
    bottomBody->setDynamic(false);
    bottomBody->setCollisionBitmask(WALL_COLLISION_BITMASK);
    bottomBody->setContactTestBitmask(true);
    bottomSprite->setPhysicsBody(bottomBody);
    
    
    //float random = 1;
    
    //auto topWallPosition = (visibleSize.height + (visibleSize.height/2) * random );// +(topSprite->getContentSize().height );
    
    topSprite->setPosition(Point(0,wallSize.height/2 + (HERO_SIZE * WALL_GAP)/2));
    bottomSprite->setPosition(Point(0,  - wallSize.height/2 - (HERO_SIZE * WALL_GAP)/2));

    
    this->addChild(topSprite, 1,1);
    this->addChild(bottomSprite,1,1);
    
    this->setPosition(origin.x+visibleSize.width+wallSize.width, posY);
    
    gameScene->addChild(this, 1, 2);

    free(buffer);
    //delete buffer;
    tex->release();
    
}

void Wall::cleanupSprite(Sprite *inSprite)
{
	// call your destroy particles here
	// remove the sprite
	inSprite->removeFromParentAndCleanup(true);
	//inSprite->release();
	log("sprite removed!");
	
}

void Wall::setGap(int gap)
{
    }


Wall::~Wall()
{
    delete heroRect;
    //delete tex;
    //log("wall class destructor!");
}
