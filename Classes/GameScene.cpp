#include "GameScene.h"
#include "Button.h"
#include "GameOverLayer.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

unsigned int starscore;

Game::Game(){
	
}
Game::~Game(){
    //log("Game scene released");
}
Scene* Game::createScene()
{
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_NONE); //DEBUGDRAW_NONE
    scene->getPhysicsWorld()->setGravity(Vect(0, 0)); //-GRAVITY_BIAS*Director::getInstance()->getVisibleSize().height));
    scene->getPhysicsWorld()->setSubsteps(16);
	
    auto layer = Game::create();
    layer->SetPhysicsWorld(scene->getPhysicsWorld());
	
    scene->addChild(layer);

    return scene;
}

// on "init" you need to initialize your instance
bool Game::init()
{
    
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    
	if (!LayerColor::initWithColor(Color4B(55, 55, 55, 255)))//Color4B(0, 0, 0, 255)))
    {
        return false;
    }

    visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    layerPos = this->getPosition();
    
	int fontSize = 24;
    
    /////////score
    
	scoreLabel = Label::createWithTTF("0", "Aller_Bd.ttf", fontSize);
    scoreLabel->setDimensions(100, 24);
	scoreLabel->setAlignment(TextHAlignment::LEFT);
    scoreLabel->setAnchorPoint(Vec2(0,0.5));	scoreLabel->setPosition(Vec2(origin.x + scoreLabel->getContentSize().height, origin.y + visibleSize.height - scoreLabel->getContentSize().height));
    this->addChild(scoreLabel,3);
    
    /////////tutorial
    
    tutorialLabel = Label::createWithTTF("TOUCH AND HOLD, RELEASE", "Aller_Bd.ttf", fontSize-10);
    tutorialLabel->setAlignment(TextHAlignment::CENTER);
    tutorialLabel->setPosition(Vec2(visibleSize.width/2, origin.y + visibleSize.height - scoreLabel->getContentSize().height*3));
    tutorialLabel->setColor(Color3B::BLACK);
    tutorialLabel->setOpacity(0);
    this->addChild(tutorialLabel,3);
    tutorialLabel->runAction(FadeIn::create(1));
    
    ////////display available collected stars
    
    //Number of stars collected
    starscore = UserDefault::getInstance()->getIntegerForKey("STARS", 0);
    UserDefault::getInstance()->flush();

    
    auto starScoreSprite = Sprite::create("hexagon.png");
    starScoreSprite->setPosition(Vec2(visibleSize.width - starScoreSprite->getContentSize().width/1.5, origin.y + visibleSize.height - scoreLabel->getContentSize().height));
    starScoreSprite->setScale(0.5);
    starScoreSprite->setColor(Color3B::YELLOW);
    this->addChild(starScoreSprite,3);
    
    char starsString[20];
    sprintf(starsString, "%d", starscore);
    
    starsLabel = Label::createWithTTF(starsString, "Aller_Bd.ttf", 24);
    starsLabel->setDimensions(100, 24);
    starsLabel->setAnchorPoint(Vec2(0.5,0.5));
    starsLabel->setAlignment(TextHAlignment::RIGHT);
    starsLabel->setPosition(Vec2(visibleSize.width - starScoreSprite->getContentSize().width*2, origin.y + visibleSize.height - scoreLabel->getContentSize().height));
    starsLabel->setColor(Color3B(0, 0, 0));
    this->addChild(starsLabel,3);
    
    ///////edge physcs body - detects hero fall
    
    auto edgeBody = PhysicsBody::createEdgeBox(visibleSize-Size(30,30), PhysicsMaterial(0.0f, 0.0f, 0.0f));
    edgeBody->setCollisionBitmask(EDGE_COLLISION_BITMASK);
    edgeBody->setContactTestBitmask(EDGE_COLLISION_BITMASK);
    //edgeBody->setCategoryBitmask(0X01);
    auto edgeNode = Node::create();
    edgeNode->setPosition(origin+ visibleSize/2);
    edgeNode->setPhysicsBody(edgeBody);
    this->addChild(edgeNode,0,3);
    
    //////poles
    
    float distance = visibleSize.width+visibleSize.width*WALL_WIDTH*2;
    float posY = origin.y+visibleSize.height/2;
    for (int i=0;i<visibleSize.width+visibleSize.width*WALL_WIDTH*2; i+=visibleSize.width*WALL_WIDTH){
        Wall *wall = new Wall(this, posY, 0, WALL_GAP, WALL_COLOR);
        wall->setPositionX(i);
        auto cleanupWall = CallFunc::create(std::bind(&Game::cleanupNode, this, wall));
        wall->runAction(Sequence::create(MoveBy::create(visibleSize.width*WALL_MOVEMENT_SPEED, Vec2(-distance,0)), cleanupWall,nullptr));
    }
    
    float wallSpawnSchedule = (visibleSize.width*WALL_MOVEMENT_SPEED*WALL_WIDTH) / (1 + WALL_WIDTH*20);
    this->schedule(schedule_selector(Game::createWalls), wallSpawnSchedule);
    
    /////hero
    int selectedHero = UserDefault::getInstance()->getIntegerForKey("HERO", 0);
	hero = new Hero(this, selectedHero);

    /////touch and release listener
    flyTouchListener = EventListenerTouchOneByOne::create();
    flyTouchListener->onTouchBegan = CC_CALLBACK_2(Game::onTouchDown, this);
    flyTouchListener->onTouchEnded = CC_CALLBACK_2(Game::onTouchRelease, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(flyTouchListener, hero);

    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(Game::onContactBegin, this);
    contactListener->onContactSeperate = CC_CALLBACK_1(Game::onContactEnds, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);
    
    /////stars and power ups positions
    powerup = NULL;
    //star = NULL;
    
//    Sprite *powerSprite = Sprite::create("powerup_particle.png");
//    starsYpos[0] = HERO_SIZE * WALL_GAP/2-powerSprite->getContentSize().height/2;
//    starsYpos[1] = origin.y+visibleSize.height/2;
//    starsYpos[2] = HERO_SIZE * WALL_GAP/2-powerSprite->getContentSize().height/2;
    
    return true;
}

bool Game::onTouchDown(Touch* touch, Event *event)
{
    if (tutorialLabel->getOpacity()>=250)
    {
        tutorialLabel->runAction(FadeOut::create(0.5));
    }
    hero->fly();
    return true;
}


void Game::onTouchRelease(Touch* touch, Event *event)
{
    hero->fall();
}

void Game::createWalls(float dt){
    
    score++;
    char scoreString[20];
    sprintf(scoreString, "%d", score);
    scoreLabel->setString(scoreString);

    angle += ANGLE_RATIO;
    
    float randWall = CCRANDOM_0_1();
    if(randWall<0.05){
        angle = angle-randWall*15;
    }

    if (angle >= 360 || angle <0 )angle=0;
    
    float randomHeight = CCRANDOM_0_1()*TOP_SCREEN_WALL_THRESHOLD;
            //log("HrandomH: %f",randomHeight);
            if (randomHeight < LOWER_SCREEN_WALL_THRESHOLD)
            {
                randomHeight = LOWER_SCREEN_WALL_THRESHOLD;
            }
    
    float y = sin(angle);
    
    if (y < LOWER_SCREEN_WALL_THRESHOLD)
    {
        y = LOWER_SCREEN_WALL_THRESHOLD;
    }else if (y > TOP_SCREEN_WALL_THRESHOLD)
    {
        y = TOP_SCREEN_WALL_THRESHOLD;
    }
    
    //log("Y: %f",y);
    
    
    //generate walls
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    float distance = visibleSize.width*WALL_WIDTH;
    float posY = origin.y+visibleSize.height/2 + (visibleSize.height/5)* y;
    Wall *wall = new Wall(this, posY, 0, WALL_GAP, WALL_COLOR);
    auto cleanupWall = CallFunc::create(std::bind(&Game::cleanupNode, this, wall));
    wall->runAction(Sequence::create(MoveTo::create(visibleSize.width*WALL_MOVEMENT_SPEED, Vec2(-distance,posY)), cleanupWall,nullptr));
    
    //generate stars
    if(CCRANDOM_0_1()<0.05 && genStarSequence){
        popStar = true;
        genStarSequence = false;
        starsYpos = int(CCRANDOM_0_1()*3);
        if(starsYpos == 3)starsYpos =2;
    }
    if(popStar){
        starN+=1;
        if(starN % STAR_GAP == 0 && starN <= STAR_GAP * STAR_SEQUENCE_GROUP){
            Star *star = new Star(this, Vec2(origin.x+visibleSize.width+distance, posY), starsYpos, WALL_GAP, STAR_COLOR);
            auto cleanStar = CallFunc::create(std::bind(&Game::cleanupNode, this, star));
            star->runAction(Sequence::create(MoveTo::create(visibleSize.width*WALL_MOVEMENT_SPEED, Vec2(-distance,star->getPositionY())), cleanStar,nullptr));
        }else if (starN > STAR_GAP * STAR_SEQUENCE_GROUP){
            genStarSequence = true;
            popStar = false;
            starN=0;
        }
    }
    
    //generate powerups
    if(/*CCRANDOM_0_1()<0.01 &&*/ !popStar && score>nextPowerUp ){
        nextPowerUp = score + 150;
        int powerType = int(CCRANDOM_0_1()*3);
        if(powerType == 3)powerType =2;
        //log("POWER_TYPE: %d",powerType);
        powerup = new Powerup(this, Vec2(origin.x+visibleSize.width+distance, posY),powerType,WALL_GAP);
        auto cleanupPower = CallFunc::create(std::bind(&Game::cleanupNode, this, powerup));
        powerup->runAction(Sequence::create(MoveTo::create(visibleSize.width*WALL_MOVEMENT_SPEED, Vec2(-distance,powerup->getPositionY())), cleanupPower,nullptr));
    }
    
    //log("INT: %d",int(CCRANDOM_0_1()*2));

    

}

bool Game::onContactBegin(PhysicsContact& contact)
{
    //char scoreString[20];
    PhysicsBody *A = contact.getShapeA()->getBody();
    PhysicsBody *B = contact.getShapeB()->getBody();
    
    if ((HERO_COLLISION_BITMASK == A->getCollisionBitmask() && WALL_COLLISION_BITMASK == B->getCollisionBitmask()) || (WALL_COLLISION_BITMASK == A->getCollisionBitmask() && HERO_COLLISION_BITMASK == B->getCollisionBitmask()))
    {
        //log("contact wall/hero");
        _eventDispatcher->removeAllEventListeners();
        hero->crash();
        this->unscheduleAllCallbacks();
        for (int i = 0; i < this->getChildren().size(); i++){
            this->getChildren().at(i)->stopAllActions();
        }
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Crash.wav");
        this->schedule(schedule_selector(Game::shakeScreen),WALL_SPAWN_FREQUENCY/10);
        
    }
    
    if ((EDGE_COLLISION_BITMASK == A->getContactTestBitmask() && HERO_COLLISION_BITMASK == B->getCollisionBitmask()) || (HERO_COLLISION_BITMASK == A->getCollisionBitmask() && EDGE_COLLISION_BITMASK == B->getContactTestBitmask()))
    {
        log("contact edge/hero");
        
    }
    
    
    if ((STAR_COLLISION_BITMASK == A->getContactTestBitmask() && HERO_COLLISION_BITMASK == B->getCollisionBitmask()) || (HERO_COLLISION_BITMASK == A->getCollisionBitmask() && STAR_COLLISION_BITMASK == B->getContactTestBitmask()))
    {
        
        //log("contact star/hero");
        Node *block;
        if (STAR_COLLISION_BITMASK == A->getContactTestBitmask()){
            block = A->getNode();
        }
        else
            if (STAR_COLLISION_BITMASK == B->getContactTestBitmask()){
                block = B->getNode();
            }
        
        starscore+=STAR_SCORE;
        char starsString[20];
        sprintf(starsString, "%d", starscore);
        starsLabel->setString(starsString);

        cleanupNode(block);
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("GrabCoin.wav");
        starCollect = true;
        
    }
    
    if ((POWER_COLLISION_BITMASK == A->getContactTestBitmask() && HERO_COLLISION_BITMASK == B->getCollisionBitmask()) || (HERO_COLLISION_BITMASK == A->getCollisionBitmask() && POWER_COLLISION_BITMASK == B->getContactTestBitmask()))
    {
        
        //log("contact star/hero");
        Node *block;
        if (POWER_COLLISION_BITMASK == A->getContactTestBitmask()){
            block = A->getNode();
        }
        else
            if (POWER_COLLISION_BITMASK == B->getContactTestBitmask()){
                block = B->getNode();
            }
        
        cleanupNode(block);
        activatePowerUp();
        
    }


    
    return true;
}

void Game::onContactEnds(PhysicsContact& contact)
{
    PhysicsBody *A = contact.getShapeA()->getBody();
    PhysicsBody *B = contact.getShapeB()->getBody();
    
    if ((WALL_SKY_COLLISION_BITMASK == A->getCollisionBitmask() && HERO_COLLISION_BITMASK == B->getCollisionBitmask()) || (HERO_COLLISION_BITMASK == A->getCollisionBitmask() && WALL_SKY_COLLISION_BITMASK == B->getCollisionBitmask()))
    {
        
    }
    
    if ((EDGE_COLLISION_BITMASK == A->getCollisionBitmask() && HERO_COLLISION_BITMASK == B->getCollisionBitmask()) || (HERO_COLLISION_BITMASK == A->getCollisionBitmask() && EDGE_COLLISION_BITMASK == B->getCollisionBitmask()))
    {
        log("EDGE");
    }
}


void Game::activatePowerUp()
{
    //log("POWERUP TYPE: %d",powerup->getType());
    
    int powerType = 0;
    
    if(powerup->getType() == 2){
        if(CCRANDOM_0_1()>0.5)powerType = 1;
    }else powerType = powerup->getType();
    
    auto up0 = CallFunc::create([=]()->void{
        
        hero->runAction(ScaleTo::create(0.5,1));

    });
    auto up1 = CallFunc::create([=]()->void{
        WALL_GAP = 4;
        WALL_COLOR = Color3B::GRAY;

    });
    auto up2 = CallFunc::create([=]()->void{
        ANGLE_RATIO = 0.125;
        WALL_COLOR = Color3B::GRAY;
    });
    auto up3 = CallFunc::create([=]()->void{
        if (genStarSequence == false && popStar == false)genStarSequence = true;
        STAR_SCORE = 1;
        STAR_COLOR = Color3B::YELLOW;

    });
        
    auto down0 = up0;
    auto down1 = up1;
    auto down2 = up2;
    auto down3 = up3;
    
    switch (powerType) {
        case 0:{
            int up = int(CCRANDOM_0_1()*4);
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("PowerUp.wav");
            log("UP: %d",up);
            switch (up) {
                case 0:
                    
                    popPowerLabel("HALF SIZE");
                    this->runAction(Sequence::create(DelayTime::create(10), up0,nullptr));
                    this->runAction(Sequence::create(TintTo::create(0.5,Color3B::GREEN), TintTo::create(9.5,Color3B(55, 55, 55)),nullptr));
                    hero->runAction(ScaleTo::create(0.5,0.5));
                    break;
                case 1:
                    popPowerLabel("BIGGER GAP");
                    WALL_GAP = 6;
                    WALL_COLOR = Color3B::GREEN;
                    this->runAction(Sequence::create(DelayTime::create(10), up1,nullptr));
                    break;
                case 2:
                    popPowerLabel("WIDE ANGLE");
                    ANGLE_RATIO = 0.075;
                    WALL_COLOR = Color3B::BLUE;
                    this->runAction(Sequence::create(DelayTime::create(10), up2,nullptr));
                    break;
                case 3:
                    popPowerLabel("HEXAGON x5");
                    if (genStarSequence == false && popStar == false)genStarSequence = true;
                    STAR_SCORE = 5;
                    STAR_COLOR = Color3B::ORANGE;
                    this->runAction(Sequence::create(DelayTime::create(10), up3,nullptr));
                    break;
                default:
                    break;
            }
            
            break;
        }
        
        case 1:{
            int down = int(CCRANDOM_0_1()*4);
            log("DOWN: %d",down);
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("PowerDown.wav");
            switch (down) {
                case 0:
                    popPowerLabel("DOUBLE SIZE");
                    this->runAction(Sequence::create(DelayTime::create(10), down0,nullptr));
                    this->runAction(Sequence::create(TintTo::create(0.2,Color3B::RED), TintTo::create(9.5,Color3B(55, 55, 55)),nullptr));
                    hero->runAction(ScaleTo::create(0.5,1.6));
                    break;
                case 1:
                    popPowerLabel("SMALLER GAP");
                    WALL_GAP = 3;
                    WALL_COLOR = Color3B::RED;
                    this->runAction(Sequence::create(DelayTime::create(10), down1,nullptr));
                    break;
                case 2:
                    popPowerLabel("NARROW ANGLE");
                    ANGLE_RATIO = 0.2;
                    WALL_COLOR = Color3B::MAGENTA;
                    this->runAction(Sequence::create(DelayTime::create(10), down2,nullptr));
                    break;
                case 3:
                    popPowerLabel("NO HEXAGONS");
                    genStarSequence = false;
                    popStar = false;
                    this->runAction(Sequence::create(DelayTime::create(10), down3,nullptr));
                    break;
                default:
                    break;
                }
            
            break;
        }
            
        default:
            break;
    }
    
    
}




void Game::cleanupNode(Node *inSprite)
{
    // call your destroy particles here
    // remove the sprite
    inSprite->removeFromParentAndCleanup(true);
    inSprite->release();
    //log("sprite removed!");
}

void Game::cleanupStar(Star *inStar)
{
    // call your destroy particles here
    // remove the sprite
    inStar->removeFromParentAndCleanup(true);
    inStar->release();
//    Star *starAux = inStar;
//    inStar = NULL;
//    delete starAux;
    //log("sprite removed!");
}

void Game::shakeScreen(float dt)
{
    float randx = rangeRandom( -visibleSize.width/100, visibleSize.width/100 );
    float randy = rangeRandom( -0.0f, 0.0 );
    
    this->setPosition(Point(randx, randy));
    this->setPosition(Point(layerPos.x + randx, layerPos.y + randy));
    
    
    
    SET_SHAKE_DURATION -= 1;
    
    if (SET_SHAKE_DURATION <= 0)
    {
        this->setPosition(layerPos);
        this->unscheduleAllCallbacks();
        
        this->GoToGameOverScene(score);
    }
}

void Game::popPowerLabel(const std::string message)
{
    //log(message);
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    auto powerLabel = Label::createWithTTF(message, "Aller_Bd.ttf", 40);
    powerLabel->setColor(Color3B::WHITE);
    powerLabel->setPosition(origin+ visibleSize/2);
    powerLabel->setOpacity(0);
    powerLabel->setScale(0.5);
    powerLabel->retain();
    this->addChild(powerLabel,100,88);
    
    auto mySpawn = Spawn::createWithTwoActions(FadeIn::create(0.5f), ScaleTo::create(0.5, 1));
    auto cleanupLabel = CallFunc::create(std::bind(&Game::cleanupNode, this, powerLabel));
    powerLabel->runAction(Sequence::create(mySpawn, FadeOut::create(2),cleanupLabel,nullptr));
    
}



float Game::rangeRandom( float min, float max )
{
    float rnd = ((float)rand()/(float)RAND_MAX);
    return rnd*(max-min)+min;
}


void Game::GoToGameOverScene(int score)
{
    //Number of stars collected
    UserDefault::getInstance()->setIntegerForKey("STARS", starscore);
    UserDefault::getInstance()->flush();
    
    //utils::captureScreen(CC_CALLBACK_2(Game::afterCaptured, this),"CaptureScreen.png");
    auto gameOverLayer = new GameOver();
    auto init = CallFunc::create([=]()->void{
        gameOverLayer->init(score);
        
    });
    this->addChild(gameOverLayer, 100);
    gameOverLayer->runAction(Sequence::create(FadeIn::create(0.5),init,nullptr));
    
}

//void Game::afterCaptured(bool succeed, const std::string& outputFile)
//{
//    if (succeed)
//    {
//        // show screenshot
//        auto sp = Sprite::create(outputFile);
//        //addChild(sp, 0, childName);
//        Size s = Director::getInstance()->getWinSize();
//        sp->setPosition(s.width / 2, s.height / 2);
//        sp->setScale(0.25);
//        log("Capture screen success.");
//    }
//    else
//    {
//        log("Capture screen failed.");
//    }
//}

void Game::onExit(){
    removeAllChildren();
    Layer::onExit();
}