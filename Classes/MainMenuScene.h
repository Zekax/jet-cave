#ifndef __MAINMENU_SCENE_H__
#define __MAINMENU_SCENE_H__

#include "cocos2d.h"

#include "ui/UIWidget.h"
#include "ui/UILayout.h"
#include "Button.h"
#include "Hero.h"
#include "Wall.h"
USING_NS_CC;

class MainMenu : public cocos2d::LayerColor
{
public:
    MainMenu();
    ~MainMenu();
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene(bool isFirst);
    void popIntersticial(float dt);
    void GoToGame(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToStore(Ref *pSender, ui::Widget::TouchEventType type);
    void GoToRate(Ref *pSender, ui::Widget::TouchEventType type);
    void Mute(Ref *pSender, ui::Widget::TouchEventType type);
    void insertObjects();
    
    virtual bool init();
    
    CREATE_FUNC(MainMenu);
    
    float rangeRandom( float min, float max )
    {
        float rnd = ((float)rand()/(float)RAND_MAX);
        return rnd*(max-min)+min;
    };
    
private:
    int score;
    unsigned int starScore;
    int selectedHero;
    Hero *hero;
    Label *starsLabel;
    Button *playButton;
    Button *muteButton;
    Button *previousHeroButton;
    Button *nextHeroButton;
    Button *unlockHeroButton;
    cocos2d::PhysicsWorld* m_world;
};

#endif