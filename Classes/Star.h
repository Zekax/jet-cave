#pragma once
#ifndef __STAR_H__
#define __STAR_H__

#include "cocos2d.h"

USING_NS_CC;

class Star: public cocos2d::Sprite
{
private:

	Size visibleSize;
    //cocos2d::ParticleSystemQuad* spiral_emitter;

public:

	Star(Layer *gameScene, Vec2 position, int vertPos, int WALL_GAP, Color3B STAR_COLOR);
	void cleanupSprite(Node *inNode);
	~Star();

};

#endif