#ifndef __Game_SCENE_H__
#define __Game_SCENE_H__

#include "cocos2d.h"
#include "Hero.h"
#include "Wall.h"
#include "Star.h"
#include "Powerup.h"
#include "ui/UIWidget.h"
#include "ui/UILayout.h"

USING_NS_CC;

class Game : public cocos2d::LayerColor
{
public:
	Game();
	~Game();
    
    static cocos2d::Scene* createScene();
    virtual bool init();
    
    void SetPhysicsWorld(cocos2d::PhysicsWorld* world){ m_world = world; };

	bool onTouchDown(Touch* touch, Event *event);
    void onTouchRelease(Touch* touch, Event *event);
    
    bool onContactBegin(cocos2d::PhysicsContact& contact);
    void onContactEnds(PhysicsContact& contact);

	//void GoToPauseScene(cocos2d::Ref *pSender);
	void GoToGameOverScene(int score);
    void createWalls(float dt);
    void cleanupNode(Node *inSprite);
    void cleanupStar(Star *inStar);
    void popPowerLabel(std::string message);
    void activatePowerUp();
    
    void shakeScreen(float dt);
    //void afterCaptured(bool succeed, const std::string& outputFile);
    void onExit();
    
    float rangeRandom( float min, float max );
    
    // implement the "static create()" method manually
    CREATE_FUNC(Game);

private:
    
    bool starCollect;
    bool popStar = false;
    bool genStarSequence = true;
    int SET_SHAKE_DURATION = 40;
    int score=0;
    int nextPowerUp=100;
    int starN=0;
    int starsYpos=0;
    int WALL_GAP = 4;
    float angle = 0;
    float ANGLE_RATIO = 0.125;
    float STAR_SCORE = 1;
    Color3B WALL_COLOR = Color3B::GRAY;
    Color3B STAR_COLOR = Color3B::YELLOW;
    
	Size visibleSize;
	Label *scoreLabel;
    Label *tutorialLabel;
    Label *starsLabel;
    
	Hero *hero;
    Powerup *powerup;
    
    cocos2d::PhysicsWorld* m_world;
    EventListenerTouchOneByOne *flyTouchListener;
    Vec2 layerPos;
//    Vector<Wall*> poles;
//    Vector<Star*> stars;
    
};

#endif // __Game_SCENE_H__
