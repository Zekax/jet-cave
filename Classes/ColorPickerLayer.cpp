#include "ColorPickerLayer.h"
#include "Definitions.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

ColorPickerLayer::ColorPickerLayer()
{
    LayerColor::initWithColor(Color4B(225, 225, 225, 2));
}


ColorPickerLayer::~ColorPickerLayer()
{
    
}

bool ColorPickerLayer::init(Store *parent)
{
    //Number of stars collected
    
    UserDefault *def = UserDefault::getInstance();
    starScore = def->getIntegerForKey("STARS", 0);
    
    int R = def->getIntegerForKey("RED", 57);
    int G = def->getIntegerForKey("GREEN", 150);
    int B = def->getIntegerForKey("BLUE", 219);
    
    color = Color3B(R,G,B);
    
    parentLayer = parent;
    
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    label = Label::createWithTTF("COLOR", "Aller_Bd.ttf", 32);
    
    label->setPosition(Vec2(origin.x + visibleSize.width / 2,origin.y + visibleSize.height - label->getContentSize().height));
    label->setTextColor(Color4B(color));
    this->addChild(label, 1);
    
    auto buttonLayout = ui::Layout::create();
    buttonLayout->setSize(visibleSize / 2);
    
    buyColorButton = new Button("   200", color, buttonLayout, origin + visibleSize/2+Vec2(250,0), 0.8, CC_CALLBACK_2(ColorPickerLayer::setColor, this), BUY, "Aller_Bd.ttf", 32, 200);

    returnButton = new Button("", color, buttonLayout, Vec2(origin.x +visibleSize.width / 2 -225, origin.y +visibleSize.height / 2 + 50), -0.2, CC_CALLBACK_2(ColorPickerLayer::ReturnToStore, this), ARROW, "Aller_Bd.ttf", 32, 200);
    
    box = Sprite::create("round_square.png");
    box->cocos2d::Node::setScale(0.25);
    box->setPosition(origin + visibleSize/2+Vec2(130,0));
    box->setColor(Color3B::WHITE);
    
    this->addChild(box);
    
    colourPicker = extension::ControlColourPicker::create();
    colourPicker->setPosition(origin + visibleSize/2);
    colourPicker->addTargetWithActionForControlEvents(this, cccontrol_selector(ColorPickerLayer::valueChanged), extension::Control::EventType::VALUE_CHANGED);
    
    this->addChild(colourPicker);
    
    this->addChild(buttonLayout,10,30);
    
    auto starScoreSprite = Sprite::create("hexagon.png");
    starScoreSprite->setPosition(Vec2(origin.x + visibleSize.width/2 +30, origin.y + visibleSize.height/5-5));
    starScoreSprite->setScale(0.5);
    starScoreSprite->setColor(Color3B::YELLOW);
    this->addChild(starScoreSprite,3);
    
    char starsString[20];
    sprintf(starsString, "%d", starScore);
    
    starsLabel = Label::createWithTTF(starsString, "Aller_Bd.ttf", 24);
    starsLabel->setDimensions(100, 24);
    starsLabel->setAnchorPoint(Vec2(1,0.5));
    starsLabel->setAlignment(TextHAlignment::RIGHT);
    starsLabel->setPosition(Vec2(origin.x + visibleSize.width/2 , origin.y + visibleSize.height/5));
    starsLabel->setColor(Color3B(0, 0, 0));
    this->addChild(starsLabel,3);

    return true;
}

void ColorPickerLayer::valueChanged(Ref *pSender, extension::Control::EventType type){
    box->setColor(colourPicker->getColor());
}

void ColorPickerLayer::setColor(Ref *pSender, ui::Widget::TouchEventType type){
    
    if(type == ui::Widget::TouchEventType::ENDED && starScore>=200){
        
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        
        UserDefault *def = UserDefault::getInstance();
        
        //Number of stars collected
        def->setIntegerForKey("RED", colourPicker->getColor().r);
        def->setIntegerForKey("GREEN", colourPicker->getColor().g);
        def->setIntegerForKey("BLUE", colourPicker->getColor().b);
        
        starScore= starScore-200;
        
        char starsString[20];
        sprintf(starsString, "%d", starScore);
        
        starsLabel->setString(starsString);
        
        //Number of stars collected
        def->setIntegerForKey("STARS", starScore);
        
        def->flush();
        
        buyColorButton->changeColor(Color3B(colourPicker->getColor().r,colourPicker->getColor().g,colourPicker->getColor().b));
        returnButton->changeColor(Color3B(colourPicker->getColor().r,colourPicker->getColor().g,colourPicker->getColor().b));
        label->setColor(Color3B(colourPicker->getColor().r,colourPicker->getColor().g,colourPicker->getColor().b));
    }
}

void ColorPickerLayer::ReturnToStore(cocos2d::Ref *pSender, ui::Widget::TouchEventType type)
{
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    if(type == ui::Widget::TouchEventType::ENDED){
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("UITap.wav");
        auto cleanUp = CallFunc::create(std::bind(&ColorPickerLayer::cleanupNode, this, this));
        if(type == ui::Widget::TouchEventType::ENDED){
            parentLayer->insertObjects();
            this->removeAllChildren();
            this->runAction(Sequence::create(FadeOut::create(1),cleanUp,nullptr));
        }
    }
    
}


