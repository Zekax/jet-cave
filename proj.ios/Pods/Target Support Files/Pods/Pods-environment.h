
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Fabric
#define COCOAPODS_POD_AVAILABLE_Fabric
#define COCOAPODS_VERSION_MAJOR_Fabric 1
#define COCOAPODS_VERSION_MINOR_Fabric 3
#define COCOAPODS_VERSION_PATCH_Fabric 0

// Fabric/Core
#define COCOAPODS_POD_AVAILABLE_Fabric_Core
#define COCOAPODS_VERSION_MAJOR_Fabric_Core 1
#define COCOAPODS_VERSION_MINOR_Fabric_Core 3
#define COCOAPODS_VERSION_PATCH_Fabric_Core 0

// Fabric/Crashlytics
#define COCOAPODS_POD_AVAILABLE_Fabric_Crashlytics
#define COCOAPODS_VERSION_MAJOR_Fabric_Crashlytics 1
#define COCOAPODS_VERSION_MINOR_Fabric_Crashlytics 3
#define COCOAPODS_VERSION_PATCH_Fabric_Crashlytics 0

// FlurrySDK
#define COCOAPODS_POD_AVAILABLE_FlurrySDK
#define COCOAPODS_VERSION_MAJOR_FlurrySDK 6
#define COCOAPODS_VERSION_MINOR_FlurrySDK 4
#define COCOAPODS_VERSION_PATCH_FlurrySDK 0

// FlurrySDK/FlurrySDK
#define COCOAPODS_POD_AVAILABLE_FlurrySDK_FlurrySDK
#define COCOAPODS_VERSION_MAJOR_FlurrySDK_FlurrySDK 6
#define COCOAPODS_VERSION_MINOR_FlurrySDK_FlurrySDK 4
#define COCOAPODS_VERSION_PATCH_FlurrySDK_FlurrySDK 0

// Google-Mobile-Ads-SDK
#define COCOAPODS_POD_AVAILABLE_Google_Mobile_Ads_SDK
#define COCOAPODS_VERSION_MAJOR_Google_Mobile_Ads_SDK 7
#define COCOAPODS_VERSION_MINOR_Google_Mobile_Ads_SDK 2
#define COCOAPODS_VERSION_PATCH_Google_Mobile_Ads_SDK 2

