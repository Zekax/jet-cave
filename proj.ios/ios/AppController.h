#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppController : NSObject <UIApplicationDelegate> {
    UIWindow *window;
}
//@property (nonatomic, strong) GADInterstitial * interstitial;
@property(nonatomic, readonly) RootViewController* viewController;
- (void) hideAdmobBanner;
- (void) showAdmobBanner;
- (void) shareButtonPressed:( int )points;
- (void) showInterstitial;
- (void) initInterstitial;
- (void) showGameCenter;
- (void) reportScore: (int64_t) score;
- (void) rate;
- (void) restoreIAP;
- (BOOL) adsRemoval;
@end

