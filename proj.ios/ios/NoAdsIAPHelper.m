//
//  NoAdsIAPHelper.m
//  JetCave
//
//  Created by Jose Rodrigues on 11/03/15.
//
//

#import "NoAdsIAPHelper.h"

@implementation NoAdsIAPHelper

+ (NoAdsIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static NoAdsIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"com.joserodrigues.JetCave.AdsRemoval2",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end