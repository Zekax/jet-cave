//
//  iOSHelper.m
//  JetCave
//
//  Created by Jose Rodrigues on 31/01/15.
//
//

#include "iOSHelper.h"
#include "AppController.h"
#import "NoAdsIAPHelper.h"

void iOSHelper::hideAdmobBanner(){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate hideAdmobBanner];
}

void iOSHelper::showAdmobBanner(){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate showAdmobBanner];
}

void iOSHelper::share(int points){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate shareButtonPressed:points];
};

void iOSHelper::initInterstitial(){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate initInterstitial];
};

void iOSHelper::showInterstitial(){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate showInterstitial];
};

void iOSHelper::showGameCenter(){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate showGameCenter];
};

void iOSHelper::reportScore(int64_t score){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate reportScore: score];
};

void iOSHelper::rate(){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate rate];
};

void iOSHelper::buyNoAds(){
    //    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    //    [appDelegate buyNoAds];
    [[NoAdsIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        
        if (success && [products count]>0) {
            SKProduct *product = products[0];
            
            NSLog(@"Buying %@...", product.productIdentifier);
            [[NoAdsIAPHelper sharedInstance] buyProduct:product];
        }else{
            NSLog(@"Product Not Available");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Service unavailable!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        //[self.refreshControl endRefreshing];
    }];
};

void iOSHelper::restoreIAP(){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [appDelegate restoreIAP];
};

bool iOSHelper::adsRemoval(){
    AppController *appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    return [appDelegate adsRemoval];
};
