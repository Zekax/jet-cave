//
//  iOSHelper.h
//  JetCave
//
//  Created by Jose Rodrigues on 31/01/15.
//
//

#ifndef JetCave_iOSHelper_h
#define JetCave_iOSHelper_h



class  iOSHelper {
    
public:
    iOSHelper() { }
    
    // Admob
    static void hideAdmobBanner();
    static void showAdmobBanner();
    static void share(int points);
    static void initInterstitial();
    static void showInterstitial();
    static void showGameCenter();
    static void reportScore(int64_t score);
    static void buyNoAds();
    static void restoreIAP();
    static void rate();
    static bool adsRemoval();
    
};

#endif