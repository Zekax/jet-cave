//
//  BannerViewController.h
//  JetCave
//
//  Created by José Rodrigues on 26/01/15.
//
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GADBannerView.h>
#import <GoogleMobileAds/GADInterstitial.h>

@interface BannerViewController : UIViewController <GADBannerViewDelegate>

- (instancetype)initWithContentViewController:(UIViewController *)contentController;
- (void) hideBanner;
- (void) showBanner;
- (void) initInterstitial;
- (void) showInterstitial;

@end

