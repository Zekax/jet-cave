//
//  NoAdsIAPHelper.h
//  JetCave
//
//  Created by Jose Rodrigues on 11/03/15.
//
//

#ifndef JetCave_NoAdsIAPHelper_h
#define JetCave_NoAdsIAPHelper_h

#import "IAPHelper.h"

@interface NoAdsIAPHelper : IAPHelper

+ (NoAdsIAPHelper *)sharedInstance;

@end

#endif
